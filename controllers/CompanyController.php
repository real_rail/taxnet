<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 10.11.2017
 * Time: 2:40
 */

namespace app\controllers;

use app\models\Company;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class CompanyController extends ActiveController
{
    public $modelClass = 'app\models\Company';

    public function actions() {
        $actions = parent::actions();
        unset($actions['create'], $actions['index'], $actions['update']);
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $params = Yii::$app->request->get();

        if (!empty($params['inn'])){
            $data = Company::find()->where(['inn' => $params['inn'], 'kpp' => $params['kpp']])->one();

            return (!empty($data)) ? $data : [];
        }

        return new ActiveDataProvider(array(
            'query' => Company::find()
        ));
    }

    public function actionCreate()
    {
        $params = Yii::$app->request->post();
        $model = new Company();

        if ((bool) $params['type'] == true)
            $model->scenario = Company::SCENARIO_PRIVATE_BUSINESSMAN;

        else $model->scenario = Company::SCENARIO_LEGAL_ENTITY;

        $model->attributes = $params;
        try{
            if ($model->save())
                return $model->attributes;
        }
        catch (Exception $e){
            throw new BadRequestHttpException();
        }

        return [];
    }

    public function actionUpdate($id)
    {
        $params = Yii::$app->request->post();
        $model = Company::findOne($id);
        if (empty($model))
            throw new BadRequestHttpException();

        if ((bool) $params['type'] == true)
            $model->scenario = Company::SCENARIO_PRIVATE_BUSINESSMAN;

        else $model->scenario = Company::SCENARIO_LEGAL_ENTITY;

        $model->attributes = $params;
        try{
            if ($model->save())
                return $model->attributes;
        }
        catch (Exception $e){
            throw new BadRequestHttpException();
        }

        return [];
    }
}