<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Компании!</h1>
    </div>

    <div class="body-content">
        <div id="companies">
        </div>
    </div>
</div>
<?php $this->registerJsFile('@web/companies.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>