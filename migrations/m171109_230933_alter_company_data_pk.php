<?php

use yii\db\Migration;

/**
 * Class m171109_230933_alter_company_data_pk
 */
class m171109_230933_alter_company_data_pk extends Migration
{
    public function up()
    {
        $table = '{{%company_data}}';
        $this->dropPrimaryKey('company_data_pk', $table);
        $this->addColumn($table, 'id', $this->integer(11)->unsigned());
        $this->addPrimaryKey('company_data_pk', $table, ['id']);
        $this->alterColumn($table, 'id', $this->integer(11)->unsigned() .' NOT NULL AUTO_INCREMENT');
        $this->createIndex('unique_company_data', $table, ['inn', 'kpp'], true);
    }

    public function down()
    {
        $table = '{{%company_data}}';
        $this->dropIndex('unique_company_data', $table);
        $this->dropPrimaryKey('company_data_pk', $table);
        $this->dropColumn($table, 'id');
        $this->addPrimaryKey('company_data_pk', $table, ['inn', 'kpp']);
    }

}
