<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_data`.
 */
class m171109_223821_create_company_data_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_data', [
            'inn' => $this->string(12)->comment("ЮЛ - 10 цифр, ИП - 12 цифр")->notNull(),
            'kpp' => $this->string(9)->comment("ЮЛ -> 9 цифр, ИП - NULL")->null(),
            'type' => $this->boolean()->comment("0 - ЮЛ, 1 - ИП")->notNull(),
            'name' => $this->string(255)->comment("не допускается латиница+кириллица")->notNull(),
            'phone' => $this->string(10)->notNull(),
            'email' => $this->string(255)->null(),
        ]);
        $this->addPrimaryKey('company_data_pk', 'company_data', ['inn', 'kpp']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_data');
    }
}
