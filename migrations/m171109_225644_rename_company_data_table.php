<?php

use yii\db\Migration;

/**
 * Class m171109_225644_rename_company_data_table
 */
class m171109_225644_rename_company_data_table extends Migration
{
    public function up()
    {
        $this->renameTable('company_data', '{{%company_data}}');
    }

    public function down()
    {
        $this->renameTable('{{%company_data}}', 'company_data');
    }

}
