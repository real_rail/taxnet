<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 10.11.2017
 * Time: 2:43
 */

namespace app\models;


use yii\db\ActiveRecord;

class Company extends ActiveRecord
{
    const SCENARIO_LEGAL_ENTITY = 'LE';
    const SCENARIO_PRIVATE_BUSINESSMAN = 'PB';

    public static function tableName()
    {
        return '{{%company_data}}';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_LEGAL_ENTITY => ['inn', 'kpp', 'type', 'phone', 'name', 'email'],
            self::SCENARIO_PRIVATE_BUSINESSMAN => ['inn', 'type', 'phone', 'name', 'email'],
        ];
    }

    public function rules()
    {
        return [
            ['type', 'boolean'],
            [['inn', 'kpp'], 'unique', 'targetAttribute' => ['inn', 'kpp']],
            ['phone', 'match', 'pattern' => '/^[0-9]{10}$/'],
            [['name', 'email'], 'string', 'max' => 255],
            [['name', 'type', 'inn', 'phone'], 'required'],
            ['email', 'default', 'value' => null],
            ['name', 'match', 'pattern' => '/^(?:\p{Cyrillic}+|\p{Latin}+)$/u'],
            ['inn', 'match', 'pattern' => '/^[0-9]{12}$/', 'on' => self::SCENARIO_PRIVATE_BUSINESSMAN],
            ['inn', 'match', 'pattern' => '/^[0-9]{10}$/', 'on' => self::SCENARIO_LEGAL_ENTITY],
            ['kpp', 'match', 'pattern' => '/^[0-9]{9}$/', 'on' => self::SCENARIO_PRIVATE_BUSINESSMAN],
        ];
    }

    private function isNnumbers($n, $attribute)
    {
        if (!preg_match('/^[0-9]{'. $n .'}$/', $attribute)) {
            $this->addError($attribute, 'must contain exactly '. $n .' digits.');
        }
    }

    public function is12numbers($value)
    {
        $this->isNnumbers(12, $value);
    }

    public function is9numbers($value)
    {
        $this->isNnumbers(9, $value);
    }

    public function is10numbers($value)
    {
        $this->isNnumbers(10, $value);
    }

}