/**
 * Created by Раиль on 10.11.2017.
 */
var new_comp_flag = false;

function sendAjax(method, url, data, successFunc, completeFunc) {
    var params = {
        url: url,
        data: data,
        dataType: 'json',
        async: false,
        type: method,
        xhrFields: { withCredentials: true },
        success: successFunc,
        complete: completeFunc,
        error: function (error, errorType, errorText) {
            console.log(error, errorType, errorText);
            alert('Ошибка в приложении ' + errorType);
        }
    };
    jQuery.ajax(params);
}

function initTableHead() {
    var table = document.createElement('table');
    table.className = 'table table-striped table-hover table-bordered';
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');
    thead.className = 'table-info';

    var params = ['#', 'ИНН', 'КПП', 'ТИП', 'ИМЯ', 'ТЕЛЕФОН', 'ПОЧТА', '', ''];
    params.forEach(function (data, i) {
        var th = document.createElement('th');
        th.textContent = data;
        tr.append(th);
    });

    thead.append(tr);
    table.append(thead);
    $('#companies').append(table);
}

function getEditButton(id) {
    var button = document.createElement('button');
    button.className = 'btn btn-info edit-company';
    button.textContent = 'РЕДАКТИРОВАТЬ';
    button.setAttribute('company', id);
    return button;
}

function getDeleteButton(id) {
    var button = document.createElement('button');
    button.className = 'btn btn-danger remove-company';
    button.textContent = 'УДАЛИТЬ';
    button.setAttribute('company', id);
    return button;
}

function getHideButton(id) {
    var button = document.createElement('button');
    button.className = 'btn btn-warning hide-company';
    button.textContent = 'УБРАТЬ';
    if (id !== null)
        button.setAttribute('company', id);
    return button;
}

function getSaveButton(id) {
    var button = document.createElement('button');
    button.className = 'btn btn-success save-company';
    button.textContent = 'СОХРАНИТЬ';
    if (id !== null)
        button.setAttribute('company', id);
    return button;
}

function addTableField(data) {
    var tr = document.createElement('tr');
    tr.setAttribute('company', data.id);
    var td = null;
    // id
    td = document.createElement('td');
    td.textContent = data.id;
    tr.append(td);
    // inn
    td = document.createElement('td');
    td.textContent = data.inn;
    tr.append(td);
    // kpp
    td = document.createElement('td');
    td.textContent = data.kpp;
    tr.append(td);
    // type
    td = document.createElement('td');
    td.textContent = (Boolean(data.type) === true) ? 'ИП' : 'ЮЛ';
    tr.append(td);
    // name
    td = document.createElement('td');
    td.textContent = data.name;
    tr.append(td);
    // phone
    td = document.createElement('td');
    td.textContent = data.phone;
    tr.append(td);
    // email
    td = document.createElement('td');
    td.textContent = data.email;
    tr.append(td);

    //edit
    td = document.createElement('td');
    td.append(getEditButton(data.id));
    tr.append(td);
    //delete
    td = document.createElement('td');
    td.append(getDeleteButton(data.id));
    tr.append(td);

    return tr;
}

function removeTableField(id) {
    var tr = $('tr[company="'+ id +'"]');
    tr.fadeOut();
    tr.remove();
}

function initTableBody(companies) {
    var tbody = document.createElement('tbody');

    companies.forEach(function (data, i) {
        tbody.append(addTableField(data));
    });

    $('#companies').find('.table').append(tbody);
}

function initAddingCompanyButton() {
    var button = document.createElement('button');
    button.className = 'btn btn-success add-company';
    button.textContent = 'ДОБАВИТЬ';
    $('#companies').append(button);
}

function getTableInputs() {
    var tr = document.createElement('tr');
    tr.className = 'row';
    var td = null;
    var input = null;
    // inn
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'inn';
    input.type = 'number';
    td.append(input);
    tr.append(td);
    // kpp
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'kpp';
    input.type = 'number';
    input.className = 'col-md-10';
    td.append(input);
    tr.append(td);
    // type
    td = document.createElement('td');
    var select = document.createElement('select');
    select.name = 'type';
    select.className = 'col-md-2';
    var option = document.createElement('option');
    option.value = '0';
    option.textContent = 'ЮЛ';
    select.append(option);
    option = document.createElement('option');
    option.value = '1';
    option.textContent = 'ИП';
    select.append(option);
    td.append(select);
    tr.append(td);
    // name
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'name';
    input.type = 'text';
    td.append(input);
    tr.append(td);
    // phone
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'phone';
    input.type = 'number';
    td.append(input);
    tr.append(td);
    // email
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'email';
    input.type = 'email';
    td.append(input);
    tr.append(td);

    //save
    td = document.createElement('td');
    td.append(getSaveButton());
    tr.append(td);

    //hide
    td = document.createElement('td');
    td.append(getHideButton());
    tr.append(td);

    return tr;
}

function setInputData(data) {
    var tr = document.createElement('tr');
    tr.className = 'row';
    tr.setAttribute('company', data.id);

    var id = $(this).attr('company');
    var td = null;
    var input = null;
    // inn
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'inn';
    input.type = 'number';
    input.value = data.inn;
    td.append(input);
    tr.append(td);
    // kpp
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'kpp';
    input.type = 'number';
    input.value = data.kpp;
    input.className = 'col-md-10';
    td.append(input);
    tr.append(td);
    // type
    td = document.createElement('td');
    var select = document.createElement('select');
    select.name = 'type';
    select.className = 'col-md-2';
    var option = document.createElement('option');
    option.value = '0';
    option.textContent = 'ЮЛ';
    select.append(option);
    option = document.createElement('option');
    option.value = '1';
    option.textContent = 'ИП';
    select.append(option);
    td.append(select);
    tr.append(td);
    // name
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'name';
    input.type = 'text';
    input.value = data.name;
    td.append(input);
    tr.append(td);
    // phone
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'phone';
    input.type = 'number';
    input.value = data.phone;
    td.append(input);
    tr.append(td);
    // email
    td = document.createElement('td');
    input = document.createElement('input');
    input.name = 'email';
    input.type = 'email';
    input.value = data.email;
    td.append(input);
    tr.append(td);

    //save
    td = document.createElement('td');
    td.append(getSaveButton(data.id));
    tr.append(td);

    //hide
    td = document.createElement('td');
    td.append(getHideButton(data.id));
    tr.append(td);

    return tr;
}

function validateCyrillic(str) {
    return !(/[^А-ЯЁ]/i.test(str));
}

function validateLatin(str) {
    return !(/[^A-Z]/i.test(str));
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateData(data, is_new_flag) {
    if (Number(data.type) === 0)
    {
        if (String(data.inn).length !== 10){
            alert('Для ЮЛ ИНН 10 цифр');
            return false;
        }

        if (String(data.kpp).length !== 9){
            alert('Для ЮЛ КПП 9 цифр');
            return false;
        }
    }
    else {
        if (String(data.inn).length !== 12){
            alert('Для ИП ИНН 12 цифр');
            return false;
        }

        if (String(data.kpp).length !== 0){
            alert('Для ИП нет КПП');
            return false;
        }
    }

    if (String(data.phone).length !== 10){
        alert('ТЕЛЕФОН 10 цифр');
        return false;
    }

    if (String(data.email).length > 255){
        alert('ПОЧТА должна быть менее 255 символов');
        return false;
    }

    if (String(data.name).length > 255){
        alert('НАЗВАНИЕ должна быть менее 255 символов');
        return false;
    }

    if (String(data.name).length === 0){
        alert('НАЗВАНИЕ не должно быть пустым');
        return false;
    }

    if (!validateLatin(String(data.name)) && !validateCyrillic(String(data.name))){
        alert('НАЗВАНИЕ должно быть либо на латинице, либо на кириллице');
        return false;
    }

    if (String(data.email).length !== 0 && !validateEmail(String(data.email))){
        alert('ПОЧТА не валидна');
        return false;
    }

    var status = true;

    if (!is_new_flag)
        return status;

    sendAjax('get', '/api/companies', {'inn': data.inn, 'kpp': data.kpp}, function (returnedData) {
        if (returnedData.inn !== undefined){
            alert('Компания с такими данными уже существует');
            status = false;
        }
    });

    return status;
}

$(document).ready(function () {
    sendAjax('get', '/api/companies', {}, function (returnedData) {
        initTableHead();
        initTableBody(returnedData);
        initAddingCompanyButton();
    });
});

$(document).on('click', '.remove-company', function () {
    var id = $(this).attr('company');
    sendAjax('delete', '/api/companies/' + id, {}, function (returnedData) {
        removeTableField(id);
    });
});

$(document).on('click', '.add-company', function () {
    if (!new_comp_flag){
        $('#companies').find('.table').append(getTableInputs());
        new_comp_flag = true;
    }
});

$(document).on('click', '.hide-company', function () {
    if ($(this).attr('company') === 'undefined'){
        var new_comp_tr = $(this).parent().parent();
        new_comp_tr.fadeOut();
        new_comp_tr.remove();
        new_comp_flag = false;
    }
    else {
        var id = $(this).attr('company');
        sendAjax('get', '/api/companies/' + id, {}, function (returnedData) {
            var comp_tr = $(document).find('tr[company="'+ id +'"]');
            comp_tr.fadeOut();
            comp_tr.replaceWith(addTableField(returnedData));
            comp_tr.fadeIn();
        });
    }
});

$(document).on('click', '.save-company', function () {
    var data = {};
    var comp_tr = $(this).parent().parent();

    data.inn = comp_tr.find('input[name="inn"]').val();
    data.kpp = comp_tr.find('input[name="kpp"]').val();
    data.type = comp_tr.find('select[name="type"]').val();
    data.name = comp_tr.find('input[name="name"]').val();
    data.phone = comp_tr.find('input[name="phone"]').val();
    data.email = comp_tr.find('input[name="email"]').val();

    var is_new = (comp_tr.attr('company') === undefined);

    if (validateData(data, is_new) === true) {
        if (is_new === true) {
            sendAjax('post', '/api/companies', data, function (returnedData) {
                comp_tr.fadeOut();
                comp_tr.remove();
                new_comp_flag = false;
                comp_tr.parent().append(addTableField(returnedData));
            });
        }
        else {
            sendAjax('put', '/api/companies/' + Number(comp_tr.attr('company')), data, function (returnedData) {
                comp_tr.fadeOut();
                comp_tr.replaceWith(addTableField(returnedData));
                comp_tr.fadeIn();
            });
        }
    }
});

$(document).on('click', '.edit-company', function () {
    var id = $(this).attr('company');
    var comp_tr = $(document).find('tr[company="'+ id +'"]');
    comp_tr.fadeOut();
    sendAjax('get', '/api/companies/' + id, {}, function (returnedData) {
        comp_tr.replaceWith(setInputData(returnedData));
        comp_tr.fadeIn();
    });
});